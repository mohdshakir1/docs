#### Latest Release &ndash;&nbsp; `v1.6.2`

 - [**Linux** &ndash;&nbsp; `amd64` &ndash;&nbsp; `v1.6.2`](https://commento-release.s3.amazonaws.com/commento-linux-amd64-v1.6.2.tar.gz)  
   `commento-linux-amd64-v1.6.2.tar.gz`  
   <p class="sha">af0eea3033e51252e991724d68f78a26ec0b16bf986428aac5d1c74a9693e91e</p>

 - [**Source Code** &ndash;&nbsp; `v1.6.2`](https://gitlab.com/commento/commento/-/archive/v1.6.2/commento-v1.6.2.tar.gz)  
   `commento-v1.6.2.tar.gz`  
   <p class="sha">28728b24c6b5c19dce492cc751f48193129ec1160472abcdaac599719362fa61</p>

#### Previous Releases

##### `v1.5.0`

 - [**Linux** &ndash;&nbsp; `amd64`](https://commento-release.s3.amazonaws.com/commento-linux-amd64-v1.5.0.tar.gz)  
   `commento-linux-amd64-v1.5.0.tar.gz`  
   <p class="sha">ba2ce14169a9603b62460b3315dd9ac5e670f2dae3efd02b4ecb984109146a74</p>

 - [**Source Code**](https://gitlab.com/commento/commento/-/archive/v1.5.0/commento-v1.5.0.tar.gz)  
   `commento-v1.5.0.tar.gz`  
   <p class="sha">c9af6d58a24c0960be33a23c283e1048e4a67e89417f578222e7c9ffe465051e</p>

##### `v1.4.2`

 - [**Linux** &ndash;&nbsp; `amd64`](https://commento-release.s3.amazonaws.com/commento-linux-amd64-v1.4.2.tar.gz)  
   `commento-linux-amd64-v1.4.2.tar.gz`  
   <p class="sha">3bd42f865cd6a18caa0f1c2544976e9cbdf470289c099e33e847bbcb9210ec16</p>

 - [**Source Code**](https://gitlab.com/commento/commento/-/archive/v1.4.2/commento-v1.4.2.tar.gz)  
   `commento-v1.4.2.tar.gz`  
   <p class="sha">f1c847c6c4cc3243fe8341795850ed9c34332729a29328733f7687266c05ca51</p>

##### `v1.3.1`

 - No release binary available for this release.

 - [**Source Code**](https://gitlab.com/commento/commento/-/archive/v1.3.1/commento-v1.3.1.tar.gz)  
   `commento-v1.3.1.tar.gz`  
   <p class="sha">38900427282f07b5f37c6fe853724e6d741d4356a9c4b3193cbff4a6595808a4</p>

##### `v1.2.0`

 - No release binary available for this release.

 - [**Source Code**](https://gitlab.com/commento/commento/-/archive/v1.2.0/commento-v1.2.0.tar.gz)  
   `commento-v1.2.0.tar.gz`  
   <p class="sha">e41335e86addac4c7c9f31803844fbc42555feb0341e6eb137dc463f5fdb04b3</p>

##### `v1.1.3`

 - No release binary available for this release.

 - [**Source Code**](https://gitlab.com/commento/commento/-/archive/v1.1.3/commento-v1.1.3.tar.gz)  
   `commento-v1.1.3.tar.gz`  
   <p class="sha">8fe889305d8737b06190d820cc49983152f348198e57d9741d426fa58acbed0c</p>

##### `v1.0.0`

 - No release binary available for this release.

 - [**Source Code**](https://gitlab.com/commento/commento/-/archive/v1.0.0/commento-v1.0.0.tar.gz)  
   `commento-v1.0.0.tar.gz`  
   <p class="sha">4b3409efdf9ffeae75366539ac0557b35ab9d106718454bbada5c2d2fe3dabc8</p>

